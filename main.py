""" This module is the start point of the software.

Before running the software, it checks for the dependencies (pytube and
Tkinter). If they are not, the software creates a log file in the same
folder as the software executable and shut down the program.
"""
import datetime

debug = True
logs = False  # Todo: Add an option in UI to disable that.
app_name = 'YT Downloader'
running = True


def log(msg) -> None:
    """ Log a message in the console.

    :param msg: Message to display (using print builtins function).
    """
    now = datetime.datetime.now().replace(microsecond=0)

    def write_log():
        f = open('logs.txt', 'a')
        f.write(f'[{now}] ' + str(msg)
                + '\n')
        f.close()

    if debug:
        print(f'[{now}] ' + str(msg)) if not logs else write_log()


if __name__ == "__main__":

    # Looking for dependencies.
    modules = {}  # Loaded modules.
    try:
        dependencies = ('pytube', 'tkinter', 'ffmpeg')
        for i in range(len(dependencies)):
            mod = __import__(dependencies[i])
            modules.update({dependencies[i]: mod})
    except ImportError as e:
        log(e)

    from gui.interface import GUIApplication

    log('(%i) dependencies loaded.' % len(modules))

    # Running the software with loaded modules.
    root = GUIApplication(app_name).mainloop()
