""" This module manages downloads and YouTube related operations via pytube."""

from typing import Tuple, Union, Optional

from main import log
# importing the pytube module.
from pytube import YouTube
from pytube.exceptions import PytubeError


class Media(YouTube):

    def __init__(self, url: str):
        """
        :param url: HTTP URL of the video (media).
        """
        try:
            super().__init__(url)
        except PytubeError as e:
            log(e)

        self.audios = self.streams.filter(only_audio=True)
        self.videos = self.streams.filter(only_video=True)

    def data(self) -> Tuple:
        """ Represents all the important parameters of the media.

        :rtype Tuple"""
        return (self.title,
                self.author,
                self.description,
                self.length,
                self.views,
                self.rating,
                self.thumbnail_url,
                self.age_restricted,
                self.caption_tracks,
                self.captions,
                self.streams,  # 11th and last element.
                )

    def download(self, tag: int, target_folder: Union[str, None] = None,
                 file_name: Union[str, None] = None,
                 replace_existing: Optional[bool] = True) -> Union[str, None]:
        """ Download a given stream from the actual media.

        :param tag: Id tag of the Stream.
        :param target_folder: Path of the folder where the file will be
        downloaded. If None, the file will be downloaded in the default
        current working directory.
        :param file_name: Name of the file that will be downloaded. It
        should not contain the extension. If None, the default file name
        will be applied.
        :param replace_existing: Replace or not if a file with the same
        name and path exists.
        :return: Absolute path of the downloaded file with its extension
        or raise a PytubeError if it could not be downloaded.
        """
        # Get the Stream by itag.
        stream = self.streams.get_by_itag(tag)
        if stream is None:
            log('No stream matches with the tag %i' % tag)
            return None
        return stream.download(target_folder, file_name,
                               '', replace_existing)
