"""This module implements all the interface of the software."""
from tkinter import *
from tkinter.font import Font

from enum import Enum


class Theme(Enum):
    # Fonts (name, size, style)
    TAHOMA_SMALL_BOLD_FONT = {'family': 'Tahoma', 'size': 18,
                              'weight': 'bold'}
    TAHOMA_SMALL_NORMAL_FONT = {'family': 'Tahoma', 'size': 14}

    # Colors
    LIGHT_GRAY_COLOR = '#A0A0A0',
    DARK_GRAY_COLOR = '#303030'
    LIGHT_RED_COLOR = '#bf5c5e'
    RED_COLOR = '#BA0004'


def create_font(theme_font: Theme) -> Font:
    value = theme_font.value
    if not isinstance(value, dict):
        raise TypeError("The selected theme variable is not a dictionary.")
    return Font(**value)


class GUIApplication(Tk):

    def __init__(self, app_title: str):
        super(GUIApplication, self).__init__()
        self.app_title = app_title
        # Theme related variables.
        self.label_font = create_font(Theme.TAHOMA_SMALL_BOLD_FONT)
        # Setting the default window's parameters.
        self.title()
        self.geometry('1080x720')
        self.minsize(1080, 720)

        # Creating the components.
        self._init_content()
        self._init_bar()

        # Applying the theme.
        self._apply_theme()

        # Placing the components.
        self._place_components()

    def _place_components(self):
        # Bar
        self.frame_bar.pack(fill=BOTH, side=TOP)

        self.frame_bar_components.pack(fill=BOTH, side=TOP, ipady=20)
        self.canvas_download_bar.pack(fill=X, side=BOTTOM, ipady=4)

        self.label_app_name.pack(fill=X, side=LEFT, ipadx=20, padx=30)
        self.frame_bar_entry.pack(side=LEFT, ipadx=10, padx=20)
        self.entry_url.pack(side=LEFT, ipadx=150, padx=10)
        self.button_add_url.pack(side=RIGHT, ipadx=0, ipady=0, padx=0)

        # Content
        self.frame_content.pack(fill=BOTH, side=BOTTOM, expand=True, ipady=250)

    def _apply_theme(self):
        self.frame_bar_components.config(bg=Theme.RED_COLOR.value)
        self.frame_content.config(bg=Theme.DARK_GRAY_COLOR.value)
        self.frame_bar_entry.config(bg=Theme.RED_COLOR.value)
        self.label_app_name.config(bg=Theme.RED_COLOR.value,
                                   font=self.label_font, fg='#dedede')
        self.entry_url.config(bg=Theme.LIGHT_RED_COLOR.value,
                              fg='white',
                              highlightthickness=0, bd=7,
                              selectborderwidth=0, selectbackground='white',
                              justify=CENTER, relief=FLAT,
                              font=create_font(Theme.TAHOMA_SMALL_NORMAL_FONT),
                              insertbackground='white')
        self.entry_url.insert(END, 'Enter the URL of the video here')

        self.button_add_url.config(bg=Theme.LIGHT_RED_COLOR.value, fg='#eeeeee',
                                   bd=0, relief=FLAT, font=self.label_font,
                                   highlightthickness=0, justify=CENTER,
                                   activeforeground=Theme.LIGHT_RED_COLOR.value)

        self.canvas_download_bar.config(bg=Theme.LIGHT_RED_COLOR.value,
                                        height=4, highlightthickness=0)

    def _init_content(self):
        self.frame_content = Frame(master=self)

        self.listbox_url_list = Listbox(master=self.frame_content)

    def _init_bar(self):
        self.frame_bar = Frame(master=self)

        self.frame_bar_components = Frame(master=self.frame_bar)
        self.canvas_download_bar = Canvas(master=self.frame_bar)

        self.label_app_name = Label(master=self.frame_bar_components,
                                    text=self.app_title)
        self.frame_bar_entry = Frame(master=self.frame_bar_components)

        self.entry_url = Entry(master=self.frame_bar_entry)
        self.button_add_url = Button(master=self.frame_bar_entry, text='+',
                                     width=1, height=1)
